package com.aditya.unacademyprogressbar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    int currentProgress;
    int initialProgress = 0;
    EditText progress;
    Button animate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new MyAsyncTask().execute("I", "am", "Aditya");

        progress = findViewById(R.id.percentage);
        animate = findViewById(R.id.animate);
        final CircleProgressBar circleProgressBar = findViewById(R.id.custom_progress);
        circleProgressBar.setStrokeWidth(10);
        animate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (progress.getText().toString().trim().equals("")) {
                    currentProgress = 0;
                    initialProgress = 0;
                } else {
                    currentProgress = Integer.parseInt(progress.getText().toString());
                    if (currentProgress != initialProgress) {
                        circleProgressBar.setProgressWithAnimation(currentProgress);
                    }
                    initialProgress = currentProgress;
                }
            }
        });


    }
}
