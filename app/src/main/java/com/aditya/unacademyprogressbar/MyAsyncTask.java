package com.aditya.unacademyprogressbar;

import android.os.AsyncTask;
import android.util.Log;

public class MyAsyncTask extends AsyncTask<String,Void,String> {
    @Override
    protected String doInBackground(String... strings) {
        String concatString="";
        for(String str:strings){
            concatString.concat(str);
        }
        return concatString;
    }


    @Override
    protected void onPostExecute(String result) {
        Log.d("Concat String is :",result);
    }


    @Override
    protected void onPreExecute() {
        Log.d("OnPreExecuteInvoked :","true");
    }
}
